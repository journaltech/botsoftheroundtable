﻿using System;
using System.Collections.Generic;

namespace BotsOfTheRoundTable
{
	public class Rng
	{
		private Random _rng;
		private double _near0 = 0.001;
		private double _near1 = 0.999;

		public Rng()
		{
			_rng = new Random();
		}

		public Rng(int seed)
		{
			_rng = new Random(seed);
		}


		public List<int> ChooseK(int limit, int k=1)
		{
			var results = new List<int>();
			int wanted = k;
			for (int i = 0; i < limit; i++)
			{
				double pSelect = (double) wanted / (limit -i);
				if (_rng.NextDouble() < pSelect)
				{
					--wanted;
					results.Add(i);
					if (wanted == 0)
					{
						return results;
					}
				}
			}
			return results;
		}


		public bool ResolveChance(double p = 0.5)
		{
			if (p <= _near0)
			{
				return false;
			}

			if (p >= _near1)
			{
				return true;
			}
			return _rng.NextDouble() < p;
		}
	}
}