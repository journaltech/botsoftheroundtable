# Bots of the Round Table
#### A programming game based on [Resistance: Avalon](http://upload.snakesandlattes.com/rules/r/ResistanceAvalon.pdf)

To enter, create a branch in the repository, then create a folder in the repository matching your username and add any classes you want inside that folder. See the existing IKnight implementation for an example. Any class that implements IEvilKnight or IGoodKnight will enter the tournament. See IKnight.cs for explanations of the methods or Main.cs for the usages. Once your branch is finished, notify me (athomas) of your branch name and I will merge it in to the tournament branch.

Tournament rules:

- Don't share state between instances. No static class variables.
- Don't use reflection or other side channels to inspect or modify state you don't own.
