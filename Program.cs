﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace BotsOfTheRoundTable
{
	class Program
	{
		static void Main(string[] args)
		{
			List<Type> cheaters = FindClasses<ICheater>();

			List<Type> goodPlayers = FindClasses<IGoodKnight>().Where(x => !cheaters.Contains(x)).ToList();
			List<Type> evilPlayers = FindClasses<IEvilKnight>().Where(x => !cheaters.Contains(x)).ToList();

			Matrix<double> goodWins = new DenseMatrix(goodPlayers.Count, evilPlayers.Count);

			int gamesPerPair = 100000;
			var rng = new Rng();

			for (int ixGood = 0; ixGood < goodPlayers.Count; ixGood++)
			{
				Type good = goodPlayers[ixGood];
				for (int ixEvil = 0; ixEvil < evilPlayers.Count; ixEvil++)
				{
					Type evil = evilPlayers[ixEvil];

					for (int i = 0; i < gamesPerPair; i++)
					{
						GameOutcome g = PlayGame(good, evil, rng);
						if (g.GoodWin)
						{
							++goodWins[ixGood, ixEvil];
						}

						int gameLength = g.QuestsLost + g.QuestsWon;
					}
				}
			}
			goodWins *= 1.0 / gamesPerPair;
			Vector<double> winDistribution = WinDistribution(goodWins); 

			Vector<double> elo = winDistribution * 30 + 1470;
			elo = ConvergeElo(elo, goodWins);

			ShowStats(elo, goodPlayers, evilPlayers, winDistribution, goodWins);
		}

		private static GameOutcome PlayGame(Type goodType, Type evilType, Rng rng)
		{
			int knightCount = 8;
			int evilCount = 3;
			var knights = new List<IKnight>(knightCount);

			int questsWon = 0;
			int questsLost = 0;

			var evils = rng.ChooseK(knightCount, evilCount);
			int merlinIndex = evils[0];
			var merlin = new Merlin();
			while (evils.Contains(merlinIndex))
			{
				merlinIndex = rng.ChooseK(knightCount, 1)[0];
			}

			for (int i = 0; i < knightCount; i++)
			{
				var good = ConstructFromType<IGoodKnight>(goodType);
				if (good == null)
				{
					return new GameOutcome
					{
						GoodWin = false,
						Forfeit = true
					};
				}
				good.NotifyGame(i, evils.Count, knightCount);
				if (i == 0)
				{
					merlin.Interventions = good.RequestMerlinIntervention();
					merlin.NotifyEvil(evils);
				}

				if (evils.Contains(i))
				{
					IEvilKnight k = ConstructFromType<IEvilKnight>(evilType);
					if (k == null)
					{
						return new GameOutcome
						{
							GoodWin = true,
							Forfeit = true,
						};
					}
					k.NotifyGame(i, evilCount: evils.Count, totalKnights: knightCount);
					k.NotifyEvil(evils);
					k.Impersonate(good);
					knights.Add(k);
				}
				else if (i == merlinIndex)
				{
					merlin.NotifyGame(i, evilCount: evils.Count, totalKnights: knightCount);
					merlin.Impersonate(good);
					knights.Add(merlin);
				}
				else
				{
					knights.Add(good);
				}
			}

			int ladyOfTheLakeIndex = knightCount - 1;

			foreach (IKnight k in knights)
			{
				k.NotifyInspectionOutcome(InspectionOutcome.SameTeam, ladyOfTheLakeIndex, ladyOfTheLakeIndex);
			}

			HashSet<int> invalidInspectionTargets = new HashSet<int>();


			var allQuests = new List<Quest>
			{
				new Quest(3, 1, false),
				new Quest(4, 1, true),
				new Quest(4, 1, true),
				new Quest(5, 2, true),
				new Quest(5, 1, false),
			};

			int leaderPosition = -1;
			foreach (Quest quest in allQuests)
			{
				//Choose a team
				int anarchy = -5;
				List<int> team = null;
				while (anarchy < 0)
				{
					leaderPosition++;
					if (leaderPosition == knights.Count)
					{
						leaderPosition = 0;
					}

					team = knights[leaderPosition].ProposeTeam(quest, anarchy).ToList();
					if (!IsTeamValid(team, quest, knightCount))
					{
						return new GameOutcome
							{
								Forfeit = true,
								GoodWin = evils.Contains(leaderPosition),
								QuestsWon = questsWon,
								QuestsLost = questsLost,
							};
					}
					team.Sort();

					//Vote on the proposed team
					List<bool> votes = knights.Select(
						k => k.VoteOnTeam(quest, team, leaderPosition, anarchy)
					).ToList();
					int yea = votes.Select(x => x ? 1 : 0).Sum();


					bool pass = 2 * yea > knights.Count;
					foreach (IKnight k in knights)
					{
						k.NotifyVotes(quest, votes, pass);
					}

					if (pass)
					{
						//Face the quest
						int totalBetrayal = team.Select(x =>knights[x].PerformQuest())
							.Select(x => x ? 0 : 1).Sum();
						if (totalBetrayal >= quest.BetrayalThreshold)
						{
							quest.Fail();
							++questsLost;
						}
						else
						{
							quest.Pass();
							++questsWon;
						}

						break;
					}

					team = null;
					++anarchy;
				}

				if (anarchy >= 0)
				{
					//anarchy prevails
					return new GameOutcome
					{
						GoodWin = false,
						QuestsWon = questsWon,
						QuestsLost = questsLost
					};
				}

				if (questsWon >= 3)
				{
					IEvilKnight assassin = knights[evils[0]] as IEvilKnight;
					if (assassin.GuessMerlin() == merlinIndex)
					{
						return new GameOutcome
						{
							GoodWin = false,
							QuestsWon = questsWon,
							QuestsLost = questsLost,
							Assassination = true
						};
					}
					else
					{
						return new GameOutcome
						{
							GoodWin = true,
							QuestsWon = questsWon,
							QuestsLost = questsLost
						};
					}
				}

				if (questsLost >= 3)
				{
					return new GameOutcome
					{
						GoodWin = false,
						QuestsWon = questsWon,
						QuestsLost = questsLost
					};
				}

				foreach (IKnight k in knights)
				{
					k.NotifyQuestCompletion(quest, team);
				}

				if (quest.Inspection)
				{
					invalidInspectionTargets.Add(ladyOfTheLakeIndex);
					IKnight inspector = knights[ladyOfTheLakeIndex];
					int targetIndex = inspector.ChooseInspectionTarget();
					while (invalidInspectionTargets.Contains(targetIndex))
					{
						targetIndex = rng.ChooseK(knightCount)[0];
					}
					InspectionOutcome result;
					if (inspector is IEvilKnight evilInspector)
					{
						result = evilInspector.ChooseInspectionOutcome(targetIndex);
					}
					else
					{
						if (evils.Contains(targetIndex))
						{
							result = InspectionOutcome.Accusation;
						}
						else
						{
							result = InspectionOutcome.SameTeam;
						}
					}

					foreach (var k in knights)
					{
						k.NotifyInspectionOutcome(result, ladyOfTheLakeIndex, targetIndex);
					}

					ladyOfTheLakeIndex = targetIndex;
				}
			}

			//This should be impossible
			return new GameOutcome
			{
				GoodWin = false,
				QuestsWon = questsWon,
				QuestsLost = questsLost
			};
		}

		private static List<Type> FindClasses<T>()
		{
			return AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
				.Where(x => typeof(T).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).ToList();
		}

		private static T ConstructFromType<T>(Type knightType)
		{
			var k = default(T);
			ConstructorInfo ci =
				knightType.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, new Type[] {}, null);
			if (ci != null)
			{
				k = (T)ci.Invoke(new object[] { });
			}

			return k;
		}

		private static bool IsTeamValid(List<int> team, Quest quest, int knightCount)
		{
			if (team.Count != quest.KnightsNeeded)
			{
				return false;
			}
			foreach (int i in team)
			{
				if (i < 0)
				{
					return false;
				}
				if (i >= knightCount)
				{
					return false;
				}
			}
			if (new HashSet<int>(team).Count != team.Count)
			{
				return false;
			}

			return true;
		}

		private static List<int> FixTeam(List<int> team, IQuest quest, int knightCount)
		{
			var t = new HashSet<int>();
			if (team != null)
			{
				t.UnionWith(team.Where(x => 0 <= x && x < knightCount));
			}

			int i = 0;
			while (t.Count < quest.KnightsNeeded)
			{
				t.Add(i);
				++i;
			}

			return t.ToList();
		}

		private static Vector<double> WinDistribution(Matrix<double> goodWins)
		{
			int evilCount = goodWins.ColumnCount;
			int goodCount = goodWins.RowCount;
			int totalCount = goodCount + evilCount;
			Matrix<double> evilWins = 1.0 - goodWins.Transpose();

			Matrix<double> transitionMatrix = new DenseMatrix(totalCount);

			transitionMatrix.SetSubMatrix(0, goodCount, (1 - goodWins) / evilCount);
			transitionMatrix.SetSubMatrix(goodCount, 0, (1 - evilWins) / goodCount);
			transitionMatrix.SetDiagonal(1.0 - transitionMatrix.RowSums());

			Vector<double> winDistribution = new DenseVector(totalCount);
			winDistribution += 1.0;

			for (int i = 0; i < 1000; i++)
			{
				winDistribution *= transitionMatrix;
			}

			return winDistribution;
		}

		private static Vector<double> ConvergeElo(Vector<double> elo,
			Matrix<double> goodWins)
		{
			int goodCount = goodWins.RowCount;
			int evilCount = goodWins.ColumnCount;

			var totalPlayers = goodCount + evilCount;
			Vector<double> dElo = new DenseVector(totalPlayers);

			double k_factor = 40;
			Matrix<double> expectedWin = new DenseMatrix(goodCount, evilCount);

			for (int i = 0; i < 2000; i++)
			{
				expectedWin.MapIndexedInplace((g, e, _) =>
				{
					double d = elo[e + goodCount] - elo[g];
					return 1.0 / (1 + Math.Exp(d / 182));
				});
				dElo.SetSubVector(0, goodCount, (goodWins - expectedWin).RowSums());
				dElo.SetSubVector(goodCount, evilCount, (expectedWin - goodWins).ColumnSums());
				elo += dElo * k_factor;
			}
			return elo;
		}

		private static void ShowStats(Vector<double> elo, List<Type> goodPlayers, List<Type> evilPlayers, Vector<double> winDistribution, Matrix<double> goodWins)
		{
			int goodPlayersCount = goodWins.RowCount;
			int evilPlayersCount = evilPlayers.Count;

			var goodElo = elo.SubVector(0, goodPlayersCount);
			goodElo += 1500 - goodElo.Average();

			var evilElo = elo.SubVector(goodPlayersCount, evilPlayersCount);
			evilElo += 1500 - evilElo.Average();

			var goodDistribution = winDistribution.SubVector(0, goodPlayersCount);
			goodDistribution *= goodPlayersCount / goodDistribution.Sum();
			var evilDistribution = winDistribution.SubVector(goodPlayersCount, evilPlayersCount);
			evilDistribution *= evilPlayersCount / evilDistribution.Sum();
			Console.WriteLine("Good rankings");
			Console.WriteLine("player, ELO, Win distribution");
			for (int i = 0; i < goodPlayersCount; i++)
			{
				Console.WriteLine($"{goodPlayers[i].Name,-26}, {goodElo[i],4:F0}, {goodDistribution[i],6:F4}");
			}

			Console.WriteLine();
			Console.WriteLine("Evil rankings");
			Console.WriteLine("player, ELO, Win distribution");
			for (int i = 0; i < evilPlayersCount; i++)
			{
				Console.WriteLine($"{evilPlayers[i].Name,-26}, {evilElo[i],4:F0}, {evilDistribution[i],6:F4}");
			}

			Console.WriteLine();
			Console.WriteLine("Combined rankings (no handicap)");
			Console.WriteLine("player, ELO, Win distribution");
			for (int i = 0; i < goodPlayersCount; i++)
			{
				Console.WriteLine($"G_{goodPlayers[i].Name,-26}, {elo[i],4:F0}, {winDistribution[i],6:F4}");
			}

			for (int i = 0; i < evilPlayersCount; i++)
			{
				var j = i + goodPlayersCount;
				Console.WriteLine($"E_{evilPlayers[i].Name,-26}, {elo[j],4:F0}, {winDistribution[j],6:F4}");
			}

			Console.WriteLine();
			Console.WriteLine(goodWins);
		}
	}

	public class GameOutcome
	{
		public bool GoodWin;
		public int QuestsWon;
		public int QuestsLost;
		public bool Forfeit;
		public bool Assassination;
	}
}
