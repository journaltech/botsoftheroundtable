﻿namespace BotsOfTheRoundTable
{
	public class Quest : IQuest
	{
		public Quest(int knightsNeeded, int betrayalThreshold, bool ladyOfTheLake)
		{
			KnightsNeeded = knightsNeeded;
			BetrayalThreshold = betrayalThreshold;
			Completed = false;
			Succeeded = false;
			Inspection = ladyOfTheLake;
		}

		public int KnightsNeeded { get; }
		public int BetrayalThreshold { get; }
		public bool Completed { get; private set; }
		public bool Succeeded { get; private set; }
		public bool Inspection { get; private set; }

		public void Fail()
		{
			Completed = true;
			Succeeded = false;
		}

		public void Pass()
		{
			Completed = true;
			Succeeded = true;
		}
	}
}