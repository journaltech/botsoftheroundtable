﻿using System.Collections.Generic;
using System.Linq;

namespace BotsOfTheRoundTable
{
	class Merlin: IGoodKnight, ICheater
	{
		private IKnight _normal;
		private Rng _rng;
		private int _totalKnights;
		public int Interventions;
		private bool _firstRound;
		private HashSet<int> _evils;

		public void NotifyGame(int yourIndex, int evilCount, int totalKnights)
		{
			_rng = new Rng();
			_totalKnights = totalKnights;
			_firstRound = true;
		}

		public ISet<int> ProposeTeam(IQuest quest, int anarchy)
		{
			var team= _normal.ProposeTeam(quest, anarchy);
			if (WouldLose(team, quest) && Interventions > 0 && !_firstRound)
			{
				--Interventions;
				team = new HashSet<int>(_rng.ChooseK(_totalKnights, quest.KnightsNeeded));
				while (WouldLose(team, quest))
				{
					team = new HashSet<int>(_rng.ChooseK(_totalKnights, quest.KnightsNeeded));
				}
			}

			return team;
		}

		private bool WouldLose(IEnumerable<int> team, IQuest quest)
		{
			return _evils.Intersect(team).Count() >= quest.BetrayalThreshold;
		}

		public bool VoteOnTeam(IQuest quest, IReadOnlyList<int> team, int leader, int anarchy)
		{
			var approval = _normal.VoteOnTeam(quest, team, leader, anarchy);

			if (approval && !_firstRound && Interventions > 0 && anarchy < -1)
			{
				if (WouldLose(team, quest))
				{
					-- Interventions;
					return false;
				}
			}

			return approval;
		}

		public void NotifyVotes(IQuest quest, IReadOnlyList<bool> votes, bool pass)
		{
			_normal.NotifyVotes(quest, votes, pass);
		}

		public bool PerformQuest()
		{
			return true;
		}

		public void NotifyQuestCompletion(IQuest quest, IReadOnlyList<int> team)
		{
			_normal.NotifyQuestCompletion(quest, team);
			_firstRound = false;
		}

		public void NotifyInspectionOutcome(InspectionOutcome outcome, int inspector, int target)
		{
			_normal.NotifyInspectionOutcome(outcome, inspector, target);
		}

		public int RequestMerlinIntervention()
		{
			return 2;
		}

		public int ChooseInspectionTarget()
		{
			return _normal.ChooseInspectionTarget();
		}

		public void NotifyEvil(IReadOnlyList<int> evils)
		{
			_evils = new HashSet<int>(evils);
		}

		public void Impersonate(IKnight goodKnight)
		{
			_normal = goodKnight;
		}
	}
}
