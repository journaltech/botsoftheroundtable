﻿using System.Collections.Generic;

namespace BotsOfTheRoundTable
{
	public enum InspectionOutcome
	{
		SameTeam,
		Accusation
	}

	public interface IKnight
	{
		/// <summary>
		/// Notify this knight of game parameters.
		/// </summary>
		/// This method is the first called after construction
		/// <param name="yourIndex">The position of this knight in any lists.</param>
		/// <param name="evilCount">How many knights serve Mordred.</param>
		/// <param name="totalKnights">How many knights are at the round table.</param>
		void NotifyGame(int yourIndex, int evilCount, int totalKnights);

		/// <summary>
		/// Get a proposed team from this knight.
		/// </summary>
		/// The current leader must populate a list of indexes for the next team.
		/// The returned list must be valid or that player forfeits the game.
		/// <param name="quest">the quest that needs the team</param>
		/// <param name="anarchy">
		/// anarchy is negative and increments each time consensus fails.
		/// at zero anarchy the game immediately fails
		/// When a team is approved anarchy resets to -5
		/// -1 anarchy means this is the final proposal before automatic failure
		/// </param>
		/// <returns> a list of player indexes </returns>
		ISet<int> ProposeTeam(IQuest quest, int anarchy);

		/// <summary>
		///  Get this knight's vote on the proposed team.
		///  </summary>
		///  All knights are told of the proposed team and may vote on it.
		///  <param name="quest">the quest this team faces</param>
		///  <param name="anarchy">
		///  anarchy is negative and increments each time consensus fails.
		///  at zero anarchy the game immediately fails
		///  When a team is approved anarchy resets to -5
		///  -1 anarchy means this is the final proposal before automatic failure
		///  </param>
		///  <returns> true if this knight supports the proposed team</returns>
		bool VoteOnTeam(IQuest quest, IReadOnlyList<int> team, int leader, int anarchy);

		/// <summary>
		/// Notify this knight of the results of the voting.
		/// </summary>
		/// <param name="quest">the quest the vote was for</param>
		/// <param name="votes">how did each knight vote?</param>
		/// <param name="pass">did the vote pass?</param>
		void NotifyVotes(IQuest quest, IReadOnlyList<bool> votes, bool pass);

		/// <summary>
		/// Get this knight's valor in the latest quest.
		/// </summary>
		/// <returns>true if the knight aided in the quest</returns>
		bool PerformQuest();

		/// <summary>
		/// Notify this knight of the result of the quest.
		/// </summary>
		void NotifyQuestCompletion(IQuest quest, IReadOnlyList<int> team);

		/// <summary>
		/// Use the Lady of the Lake on a another player
		/// </summary>
		/// <returns>The index of the player to inspect.</returns>
		int ChooseInspectionTarget();

		/// <summary>
		/// Notify this knight of the result of the inspection.
		/// </summary>
		void NotifyInspectionOutcome(InspectionOutcome outcome, int inspector, int target);
	}

	public interface IGoodKnight : IKnight
	{
		/// <summary>
		/// Get the maximum number of times Merlin is allowed to intervene
		/// </summary>
		/// This method is only called on the knight in position 0.
		/// Merlin will never intervene on the first round.
		int RequestMerlinIntervention();
	}

	public interface IEvilKnight : IKnight
	{
		/// <summary>
		/// Notify this knight of which knights are evil.
		/// </summary>
		/// <param name="evils">A list of evil player indexes.</param>
		void NotifyEvil(IReadOnlyList<int> evils);

		/// <summary>
		/// Give this knight a model of expected behavior.
		/// </summary>
		/// An evil knight can know what the expected response is.
		/// This is implemented by putting the evil knight in a man-in-the-middle position.
		void Impersonate(IKnight goodKnight);

		InspectionOutcome ChooseInspectionOutcome(int target);

		/// <summary>
		/// If evil would lose then it can instead win by guessing which player is merlin.
		/// </summary>
		/// Any incorrect result is a loss for evil.
		/// This method is called on the first evil player.
		int GuessMerlin();
	}
}