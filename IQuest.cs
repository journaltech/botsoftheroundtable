﻿namespace BotsOfTheRoundTable
{
	public interface IQuest
	{
		int KnightsNeeded { get; }
		int BetrayalThreshold { get; }
		bool Completed { get;}
		bool Succeeded { get;}
	}
}