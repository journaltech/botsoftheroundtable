﻿using System.Collections.Generic;

namespace BotsOfTheRoundTable.Baseline
{
	class SirSimpleton : IGoodKnight
	{
		private Rng _rng;
		private int _totalKnights;

		public void NotifyGame(int yourIndex, int evilCount, int totalKnights)
		{
			_rng = new Rng();
			_totalKnights = totalKnights;
		}

		public ISet<int> ProposeTeam(IQuest quest, int anarchy)
		{
			return new HashSet<int>(_rng.ChooseK(_totalKnights, quest.KnightsNeeded));
		}

		public bool VoteOnTeam(IQuest quest, IReadOnlyList<int> team, int leader, int anarchy)
		{
			if (anarchy == -1)
			{
				return true;
			}

			return _rng.ResolveChance(0.55);
		}

		public void NotifyVotes(IQuest quest, IReadOnlyList<bool> votes, bool pass)
		{
		}

		public bool PerformQuest()
		{
			return true;
		}

		public void NotifyQuestCompletion(IQuest quest, IReadOnlyList<int> team)
		{
		}

		public int ChooseInspectionTarget()
		{
			return 0;
		}

		public void NotifyInspectionOutcome(InspectionOutcome outcome, int inspector, int target)
		{
		}

		public int RequestMerlinIntervention()
		{
			return 8;
		}
	}
}
