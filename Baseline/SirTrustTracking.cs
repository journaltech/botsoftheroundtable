﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BotsOfTheRoundTable.Baseline
{
	class SirTrustTracking : IGoodKnight
	{
		private Dictionary<int, double> _playersTrustRating;
		private int _index;
		private int _evilCount;
		private int _totalKnights;
		private HashSet<int> _currentTeam;
		private bool _currentVote;
		private Dictionary<int, bool> _lastVotes;

		public void NotifyGame(int yourIndex, int evilCount, int totalKnights)
		{
			this._index = yourIndex;
			this._evilCount = evilCount;
			this._totalKnights = totalKnights;
			_playersTrustRating = new Dictionary<int, double>();
			_currentTeam = new HashSet<int>();
			_lastVotes = new Dictionary<int, bool>();
			for (int i = 0; i < totalKnights; i++)
			{
				_playersTrustRating.Add(i, 1);
			}
		}

		public int RequestMerlinIntervention()
		{
			return 0;
		}

		public ISet<int> ProposeTeam(IQuest quest, int anarchy)
		{
			_currentTeam = new HashSet<int>();
			_currentTeam.Add(_index);

			using (IEnumerator<int> bestPlayers = _playersTrustRating.OrderByDescending(s => s.Value)
				.Select(s => s.Key)
				.GetEnumerator())
			{

				while (_currentTeam.Count < quest.KnightsNeeded)
				{
					bestPlayers.MoveNext();
					_currentTeam.Add(bestPlayers.Current);
				}
			}

			return _currentTeam;
		}

		public bool VoteOnTeam(IQuest quest, IReadOnlyList<int> team, int leader, int anarchy)
		{
			_currentTeam = new HashSet<int>(team);

			var lowest = GetSuspisionsKnights();

			if (leader == _index)
			{
				_currentVote = true;
				return true;
			}

			if (anarchy == -1)
			{
				_currentVote = true;
				return true;
			}

			if (anarchy == -2 && (_currentTeam.Count(c => lowest.Contains(c)) >= quest.BetrayalThreshold))
			{
				_currentVote = false;
				return false;
			}

			if (team.Any(t => lowest.Contains(t)))
			{
				_currentVote = false;
				return false;
			}

			_currentVote = true;
			return true;
		}

		private IEnumerable<int> GetSuspisionsKnights()
		{
			var highestRating = _playersTrustRating.OrderByDescending(s => s.Value).FirstOrDefault().Value;
			var lowestRating = _playersTrustRating.OrderBy(s => s.Value).FirstOrDefault().Value;

			if (Math.Abs(highestRating - lowestRating) < double.Epsilon)
				return new List<int>();

			return _playersTrustRating.Where(s => s.Key != _index && Math.Abs(s.Value - highestRating) > double.Epsilon).OrderBy(s => s.Value).Select(s => s.Key).Take(_evilCount);
		}

		public void NotifyVotes(IQuest quest, IReadOnlyList<bool> votes, bool pass)
		{
			for (int i = 0; i < votes.Count; i++)
			{
				if (pass)
				{
					if (_lastVotes.ContainsKey(i))
					{
						_lastVotes[i] = votes[i];
					}
					else
					{
						_lastVotes.Add(i, votes[i]);
					}
				}
				else
				{
					if (votes[i] && !_currentVote)
					{
						_playersTrustRating[i] = (_playersTrustRating[i] - .25) / 2;
					}
				}
			}
			return;
		}

		public bool PerformQuest()
		{
			return true;
		}

		public void NotifyQuestCompletion(IQuest quest, IReadOnlyList<int> team)
		{
			foreach (var i in team)
			{
				var successRate = (quest.Succeeded ? 1 : 0) + _playersTrustRating[i] - (_lastVotes[i] && !quest.Succeeded && !_currentVote ? 1.0 : 0.0) / 4;
				successRate = successRate / 2;

				if (!_playersTrustRating.ContainsKey(i))
				{
					_playersTrustRating.Add(i, successRate);
				}
				else
				{
					_playersTrustRating[i] = successRate;
				}
			}


			_currentTeam = null;
		}

		public int ChooseInspectionTarget()
		{
			return (_index + 1) % _totalKnights;
		}

		public void NotifyInspectionOutcome(InspectionOutcome outcome, int inspector, int target)
		{
		}
	}
}
