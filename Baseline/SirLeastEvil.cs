﻿using System.Collections.Generic;
using System.Linq;

namespace BotsOfTheRoundTable.Baseline
{
	class SirLeastEvil : IEvilKnight
	{
		private IKnight _knight;
		private int _me;
		private int _evilCount;
		private int _totalKnights;
		private Rng _rng;
		private IReadOnlyList<int> _evils;

		public void NotifyGame(int yourIndex, int evilCount, int totalKnights)
		{
			_me = yourIndex;
			_evilCount = evilCount;
			_totalKnights = totalKnights;
			_rng = new Rng();
		}

		public ISet<int> ProposeTeam(IQuest quest, int anarchy)
		{
			return _knight.ProposeTeam(quest, anarchy);
		}

		public bool VoteOnTeam(IQuest quest, IReadOnlyList<int> team, int leader, int anarchy)
		{
			return _knight.VoteOnTeam(quest, team, leader, anarchy);
		}

		public void NotifyVotes(IQuest quest, IReadOnlyList<bool> votes, bool pass)
		{
			_knight.NotifyVotes(quest, votes, pass);
		}

		public bool PerformQuest()
		{
			_knight.PerformQuest();
			return false;
		}

		public void NotifyQuestCompletion(IQuest quest, IReadOnlyList<int> team)
		{
			_knight.NotifyQuestCompletion(quest, team);
		}

		public int ChooseInspectionTarget()
		{
			return _knight.ChooseInspectionTarget();
		}

		public void NotifyInspectionOutcome(InspectionOutcome outcome, int inspector, int target)
		{
			_knight.NotifyInspectionOutcome(outcome, inspector, target);
		}

		public void NotifyEvil(IReadOnlyList<int> evils)
		{
			_evils = evils;
		}

		public void Impersonate(IKnight goodKnight)
		{
			_knight = goodKnight;
			_knight.NotifyGame(_me, _evilCount, _totalKnights);
		}

		public InspectionOutcome ChooseInspectionOutcome(int target)
		{
			if (_rng.ResolveChance(0.5))
			{
				return InspectionOutcome.SameTeam;
			}
			else
			{
				return InspectionOutcome.Accusation;
			}
		}

		public int GuessMerlin()
		{
			int i = 0;
			while (_evils.Contains(i))
			{
				i++;
			}
			return i;
		}
	}
}